import itertools
import sys
import subprocess


def cat_csv_py(file, output):
    if not file:
        sys.exit(0)

    head_file = file[0]
    tail_files_without_headers = [
        itertools.islice(f, 1, None)
        for f in file[1:]
    ]

    lines = itertools.chain(head_file, *tail_files_without_headers)

    for line in lines:
        output.write(line)


def cat_csv(filenames, output=None):
    # http://stackoverflow.com/a/16890695
    cmd = "xargs awk 'FNR==1 && NR!=1{next;}{print}'"
    file_args = " ".join(filenames)
    if file_args and file_args != '-':
        cmd = "echo {} | {}".format(file_args, cmd)
    elif sys.stdin.isatty():
        sys.exit(0)
    if output and output != "-":
        cmd = "{} > {}".format(cmd, output)
    subprocess.run(cmd, shell=True, executable="/bin/bash")
