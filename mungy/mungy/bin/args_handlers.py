import click


def handle_column_indices(param, value):
    try:
        value = [int(c) for c in value]
    except ValueError as e:
        msg = ("'{}' comma separated values must be integers "
               "if no header is used ({})").format(param, e)
        raise click.BadParameter(msg)
    return value


def handle_no_header_label(label_column):
    try:
        label_column = int(label_column) if label_column else 0
    except ValueError as e:
        msg = ("label column must be integer index "
               "if no header is used ({})").format(e)
        raise click.BadParameter(msg)
    return label_column


def handle_column_delimiter(ctx, param, value):
    value = "\t" if value == "\\t" else value
    if len(value) > 1:
        msg = ("delimiter must be a 1-character string or \\t,"
               " but given: {}".format(value))
        raise click.BadParameter(msg)
    return value
