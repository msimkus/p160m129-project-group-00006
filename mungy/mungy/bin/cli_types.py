import click
import ast


FILE_TYPE = click.Path()


class FilenamesParamType(click.ParamType):
    name = "filenames"

    def convert(self, value, param, ctx):
        return [FILE_TYPE.convert(f, param, ctx) for f in value.split(",")]


class ProbabilityParamType(click.ParamType):
    name = 'probability'

    def convert(self, value, param, ctx):
        try:
            prob = float(value)
            if not 0 <= prob <= 1:
                msg = "got value {}, but it must be between 0.0 and 1.0".format(prob)
                self.fail(msg, param, ctx)
            return prob
        except ValueError:
            msg = '{} is not a valid probability value'.format(value)
            self.fail(msg, param, ctx)


class ProbabilitiesParamType(click.ParamType):
    name = 'probabilities'

    def convert(self, value, param, ctx):
        probabilities = []
        for p in value.split(","):
            try:
                prob = float(p)
                if not 0 <= prob <= 1:
                    msg = "got value {}, but it must be between 0.0 and 1.0".format(prob)
                    self.fail(msg, param, ctx)
                probabilities.append(prob)
            except ValueError:
                msg = '{} is not a valid probability value'.format(value)
                self.fail(msg, param, ctx)

        if sum(probabilities[:-1]) >= 1:
            msg = (
                "got {} as sum of probabilities {}, "
                "but sum must not exceed 1.0").format(
                sum(probabilities[:-1]),
                probabilities[:-1])
            self.fail(msg)

        return probabilities
