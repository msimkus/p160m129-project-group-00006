import csv
import functools


def csv2vw_with_header(
        input_file,
        output_file,
        col_delim,
        nested_val_delims,
        label_col,
        is_binary_label,
        ignored_columns=None,
        categorical_columns=None,
        all_categorical=False):
    if not ignored_columns:
        ignored_columns = []
    if not categorical_columns:
        categorical_columns = []
    csv_reader = csv.DictReader(input_file, delimiter=col_delim)
    if not label_col:
        label_col = csv_reader.fieldnames[0]

    vw_line_formatter = mk_vw_line_formatter(is_binary_label)

    for line in csv_reader:
        explanatory_values = (
            (
                format_categorical_feature(col, val)
                if all_categorical
                else format_feature(col, val, categorical_columns)
            )
            for col in csv_reader.fieldnames
            for val in explode_nested_value(line[col], nested_val_delims)
            if col != label_col and col not in ignored_columns and val != ''
        )
        vw_line = vw_line_formatter(
            line[label_col],
            filter(None, explanatory_values))
        output_file.write(vw_line)


def csv2vw_no_header(
        input_file,
        output_file,
        col_delim,
        nested_val_delims,
        idx_label_col,
        is_binary_label,
        ignored_column_indices=None,
        categorical_columns=None,
        all_categorical=False):
    if not categorical_columns:
        categorical_columns = []
    if not ignored_column_indices:
        ignored_column_indices = []
    if not idx_label_col:
        idx_label_col = 0

    vw_line_formatter = mk_vw_line_formatter(is_binary_label)
    csv_reader = csv.reader(input_file, delimiter=col_delim)

    for line in csv_reader:
        explanatory_values = (
            (
                format_categorical_feature(idx_col, csv_val)
                if all_categorical
                else format_feature(idx_col, csv_val, categorical_columns)
            )
            for idx_col, csv_val in enumerate(line)
            for val in explode_nested_value(csv_val, nested_val_delims)
            if
            idx_col != idx_label_col
            and idx_col not in ignored_column_indices
            and val != '')
        vw_line = vw_line_formatter(
            line[idx_label_col],
            filter(None, explanatory_values))
        output_file.write(vw_line)


def clean_val(val):
    return str(val).replace(" ", "").replace("|", "").replace(":", "")


def explode_nested_value(value, nested_value_delimiters):
    if not nested_value_delimiters:
        return [value]
    head = nested_value_delimiters[-1]
    tail = nested_value_delimiters[:-1]
    return functools.reduce(
        lambda r, d: r.replace(d, head),
        tail,
        value
    ).split(head)


def format_feature(column, value, categorical_columns=None):
    if not categorical_columns:
        categorical_columns = []
    is_float = column not in categorical_columns
    try:
        float(value)
    except ValueError:
        is_float = False
    return (
        format_numeric_feature(column, value)
        if is_float
        else format_categorical_feature(column, value))


def format_categorical_feature(column, value):
    column, value = clean_val(column), clean_val(value)
    return "{}__{}".format(column, value)


def format_numeric_feature(column, value):
    column, value = clean_val(column), clean_val(value)
    return "{}:{}".format(column, value)


def mk_vw_line_formatter(is_binary_label):
    return (
        mk_vw_line_with_binary_label
        if is_binary_label
        else mk_vw_line_with_continuous_label)


def mk_vw_line_with_binary_label(label_value, explanatory_values):
    return "{:.0f} |n {}\n".format(
        float(label_value) * 2.0 - 1.0, " ".join(explanatory_values))


def mk_vw_line_with_continuous_label(label_value, explanatory_values):
    return "{} |n {}\n".format(label_value, " ".join(explanatory_values))
