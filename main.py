import sys
import glob
import os
import subprocess
import json

from mungy import date_group


def mk_dir_output(slicing_name, country_id, dataset_index):
    return os.path.join(
        "data",
        "data_munging",
        "train_test_fnames",
        slicing_name,
        "country_{}".format(country_id),
        "{:03d}".format(dataset_index))


def mk_cmd_fnames_to_vw(path_csvfnames, output=None):
    cmd = " | ".join([
        "cat {}".format(path_csvfnames),
        "mungy catcsv ",
        (
            'mungy csv2vw'
            ' --label=impression_was_clicked'
            ' --binary-label'
            ' --header'
            ' --all-categorical'
            ' --ignored-columns=log_time'
            ' --nested-val-delims=";,"'
        ) + (" --output {}".format(output) if output else "")
    ])
    return cmd


def mk_cmd_extract_column(path_csv_filenames):
    cmd = " | ".join([
        "cat {}".format(path_csv_filenames),
        "mungy catcsv",
        'mpipe extract-column impression_was_clicked --header --delim="\t"'
    ])
    return cmd


def mk_cmd_vw_train(
        path_model, path_readable_model, path_data=None,
        L1=None, L2=None, learning_rate=None):
    cmd = " ".join([
        "vw",
        "--kill_cache",
        "--normalized",
        "--cache",
        "--passes 50",
        "--data {}".format(path_data)
        if path_data and path_data != "-" else "",
        "--final_regressor {}".format(path_model),
        "--readable_model {}".format(path_readable_model),
        "--l1 {}".format(L1) if L1 else "",
        "--l2 {}".format(L2) if L2 else "",
        "--learning_rate {}".format(learning_rate) if learning_rate else ""
    ])
    return ("rm {}.cache; ".format(path_data) if path_data else "") + cmd


def mk_cmd_vw_predict(path_model, path_predictions, path_data=None):
    cmd = " ".join([
        "vw",
        "--normalized",
        "--testonly",
        "--link=logistic",
        "--data {}".format(path_data)
        if path_data and path_data != "-" else "",
        "--initial_regressor {}".format(path_model),
        "--predictions {}".format(path_predictions),
    ])
    return ("rm {}.cache; ".format(path_data) if path_data else "") + cmd


def mk_cmd_classification_metrics(
        path_csv_filenames, path_predictions, output=None):
    cmd_extract_column = mk_cmd_extract_column(path_csv_filenames)
    cmd = " | ".join([
        cmd_extract_column,
        'paste -d " " - {}'.format(path_predictions),
        "mpipe metrics-cls" + (" --output {}".format(output) if output else "")
    ])
    return cmd


def mk_cmd_collect_metrics_output(slicing_name, country_id,
                                  L1=None, L2=None,
                                  learning_rate=None):
    cmd = " ".join([
        "ls",
        os.path.join(
            "data",
            "modelling",
            "metrics",
            slicing_name,
            "country_{}".format(country_id),
            "*",
            "metrics__day_*__l1_{}_l2_{}_lrate_{}.txt".format(
                L1 if L1 else "",
                L2 if L2 else "",
                learning_rate if learning_rate else "")),
        "| sort |",
        "mungy catcsv >",
        os.path.join(
            "data",
            "modelling",
            "metrics",
            slicing_name,
            "country_{}".format(country_id),
            "metrics__no_idx__all_l1_{}_l2_{}_lrate_{}.txt".format(
                L1 if L1 else "",
                L2 if L2 else "",
                learning_rate if learning_rate else ""))
    ])
    return cmd


# initialise parameters
slicing_params_file = sys.argv[1]

with open(slicing_params_file) as f:
    slicing_params = json.load(f)
country_id = slicing_params["country_id"]
n_training_periods = slicing_params["n_training_periods"]
training_period_type = slicing_params["training_period_type"]
n_testing_periods = slicing_params["n_testing_periods"]
testing_period_type = slicing_params["testing_period_type"]
n_window_periods = slicing_params["n_window_periods"]
window_period_type = slicing_params["window_period_type"]
min_testing_datasets = slicing_params["min_testing_datasets"]

with open("data/model_parameters/L1s.json") as f:
    L1s = json.load(f)
with open("data/model_parameters/L2s.json") as f:
    L2s = json.load(f)
with open("data/model_parameters/learning_rates.json") as f:
    learning_rates = json.load(f)

slicing_name = "train_{}{}_test_{}{}_by_{}{}".format(n_training_periods,
                                                     training_period_type[0],
                                                     min_testing_datasets,
                                                     testing_period_type[0],
                                                     n_window_periods,
                                                     window_period_type[0])

all_filenames = glob.glob(
    ("data/data_munging/data_by_country_by_hour/"
     "user_country_{}*.csv".format(country_id)))

all_filenames_pairs = date_group.make_datetime_filename_pairs(all_filenames)

ds_train = date_group.make_training_testing_datasets(
    all_filenames_pairs,
    n_training_periods=n_training_periods,
    training_period_type=training_period_type,
    n_testing_periods=n_testing_periods,
    testing_period_type=testing_period_type,
    n_window_periods=n_window_periods,
    window_period_type=window_period_type,
    min_testing_datasets=min_testing_datasets
)

ds_train_named = date_group.name_training_testing_datasets(ds_train)

os.makedirs(
    "data/data_munging/train_test_fnames/{}/country_{}".format(slicing_name,
                                                               country_id),
    exist_ok=True)

# create train and test datasets
for i, ds in enumerate(ds_train_named):

    dir_output = mk_dir_output(slicing_name, country_id, i)

    os.makedirs(dir_output, exist_ok=True)

    fname_train = "train__{}__{}.txt".format(
        ds.train_dataset.interval.start.strftime("%Y-%m-%d_%H%M"),
        ds.train_dataset.interval.end.strftime("%Y-%m-%d_%H%M"))

    path_train = os.path.join(dir_output, fname_train)

    with open(path_train, "w") as f:
        f.write("\n".join(sorted(ds.train_dataset.filepaths)))

    for j, test_ds in enumerate(ds.test_datasets[:min_testing_datasets]):

        fname_test = "test_{:03d}__{}__{}.txt".format(
            j,
            ds.train_dataset.interval.start.strftime("%Y-%m-%d_%H%M"),
            ds.train_dataset.interval.end.strftime("%Y-%m-%d_%H%M"))

        path_test = os.path.join(dir_output, fname_test)

        with open(path_test, "w") as f_test:
            f_test.write("\n".join(sorted(test_ds.filepaths)))

for i, ds in enumerate(ds_train_named):

    os.makedirs(
        ("data/data_munging/train_test_vw_datasets/"
         "{}/country_{}/{:03d}".format(slicing_name,
                                       country_id, i)),
        exist_ok=True)

    fname_train = "train__{}__{}".format(
        ds.train_dataset.interval.start.strftime("%Y-%m-%d_%H%M"),
        ds.train_dataset.interval.end.strftime("%Y-%m-%d_%H%M"))

    # convert from .txt to .vw
    cmd_format_data = mk_cmd_fnames_to_vw(
        ("data/data_munging/train_test_fnames/"
         "{}/country_{}/{:03d}/{}.txt".format(slicing_name,
                                              country_id, i,
                                              fname_train)),
        ("data/data_munging/train_test_vw_datasets/"
         "{}/country_{}/{:03d}/{}.vw".format(slicing_name,
                                             country_id, i,
                                             fname_train)))
    subprocess.run(cmd_format_data, shell=True, executable="/bin/bash")

    os.makedirs(
        "data/modelling/vw_models/{}/country_{}/{:03d}/".format(slicing_name,
                                                                country_id, i),
        exist_ok=True)

    os.makedirs(
        "data/modelling/predictions/{}/country_{}/{:03d}/".format(slicing_name,
                                                                  country_id,
                                                                  i),
        exist_ok=True)

    os.makedirs(
        "data/modelling/metrics/{}/country_{}/{:03d}/".format(slicing_name,
                                                              country_id, i),
        exist_ok=True)

    test_files_pattern = os.path.join(
        mk_dir_output(slicing_name, country_id, i), "test_*")

    test_filenames = sorted(glob.glob(test_files_pattern))

    # train models with different L1, L2 and learning rate values
    for L1 in L1s:
        for L2 in L2s:
            for learning_rate in learning_rates:

                path_model = (
                    "data/modelling/vw_models/{}/country_{}/{:03d}"
                    "/model__l1_{}_l2_{}_lrate_{}.vw".format(slicing_name,
                                                             country_id, i,
                                                             L1, L2,
                                                             learning_rate))
                path_readable_model = (
                    "data/modelling/vw_models/{}/country_{}/{:03d}/"
                    "model__l1_{}_l2_{}_lrate_{}.txt".format(slicing_name,
                                                             country_id, i,
                                                             L1, L2,
                                                             learning_rate))
                path_data_training = (
                    "data/data_munging/train_test_vw_datasets/"
                    "{}/country_{}/{:03d}/{}.vw".format(slicing_name,
                                                        country_id, i,
                                                        fname_train))

                cmd_train_vw_model = mk_cmd_vw_train(
                    path_model,
                    path_readable_model,
                    path_data_training,
                    L1,
                    L2,
                    learning_rate)
                subprocess.run(cmd_train_vw_model,
                               shell=True, check=True,
                               executable="/bin/bash")

                # predict and evaluate predictions
                for day, test_fname in enumerate(test_filenames, start=1):

                    path_predictions = (
                        "data/modelling/predictions/{}/country_{}/{:03d}/"
                        "predictions__day_{:02d}__"
                        "l1_{}_l2_{}_lrate_{}.txt").format(slicing_name,
                                                           country_id, i, day,
                                                           L1, L2,
                                                           learning_rate)

                    cmd_fnames_to_vw_test_day = mk_cmd_fnames_to_vw(test_fname)

                    cmd_predict_from_stdin = mk_cmd_vw_predict(
                        path_model,
                        path_predictions)

                    cmd_predict_test_day = "{} | {}".format(
                        cmd_fnames_to_vw_test_day,
                        cmd_predict_from_stdin)

                    path_metrics = (
                        "data/modelling/metrics/{}/country_{}/{:03d}/"
                        "metrics__day_{:02d}__"
                        "l1_{}_l2_{}_lrate_{}.txt").format(slicing_name,
                                                           country_id,
                                                           i, day,
                                                           L1, L2,
                                                           learning_rate)

                    cmd_metrics_day = mk_cmd_classification_metrics(
                        test_fname,
                        path_predictions,
                        path_metrics)

                    subprocess.run(cmd_predict_test_day,
                                   shell=True, executable="/bin/bash",
                                   check=True)
                    subprocess.run(cmd_metrics_day,
                                   shell=True, executable="/bin/bash",
                                   check=True)

# collect metrics for later analysis
for L1 in L1s:
    for L2 in L2s:
        for learning_rate in learning_rates:
            cmd_collect_metrics = mk_cmd_collect_metrics_output(slicing_name,
                                                                country_id,
                                                                L1, L2,
                                                                learning_rate)
            subprocess.run(cmd_collect_metrics,
                           shell=True,
                           executable="/bin/bash")
