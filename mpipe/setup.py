from setuptools import (
    setup,
    find_packages
)

setup(
    name="mpipe",
    version="0.1.0",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "click==6.7"
    ],
    entry_points="""
        [console_scripts]
        mpipe=mpipe.bin.mpipe_cli:mpipe_cli
    """,
)
